# Network Propagation

In this tutorial, we will use the heat diffusion algorithm implemented in the Diffusion app. 

> Q1: How does the heat diffusion algorithm work? (basics! [10.1371/journal.pcbi.1005598](https://doi.org/10.1371/journal.pcbi.1005598))

1. Download and open the [provided Cytoscape session](https://gitlab.com/mkutmon/tutorial-network-algorithms/raw/master/data/LungCancer.cys) containing a PPI network for proteins known to be involved in lung cancer. The STRING app was used to create the network.

> Q2: How many nodes and links are in the network? What does the node color indicate? 

2. Install the Diffusion App either from within Cytoscape 3 (Apps → App Manager) or directly from the Cytoscape app store.

3. Open R Studio and start a new R script:

```
BiocManager::install(c("RCy3"))
library(RCy3)
```

4. Find the most relevant proteins for lung cancer based on the disease score and highlight them in the network:

```
clearSelection()
copyVisualStyle("default", "diffusion")
setVisualStyle("diffusion")
deleteStyleMapping("diffusion","NODE_FILL_COLOR")
disease.score.table <- getTableColumns('node','disease score')
top.nodes <- row.names(disease.score.table)[disease.score.table$`disease score` == 5.0]
selectNodes(top.nodes)
setNodeBorderColorBypass(top.nodes,"#FF55AA")
setNodeBorderWidthBypass(top.nodes,5)
```

> Q3: How many nodes are selected? Based on what criteria have they been selected?

5. Starting from the most relevant genes, use a network propagation algorithm to identify most likely candidate genes.

```
diffusionBasic()
clearSelection()
setNodeColorMapping("diffusion_output_heat", c(0,0.5), c("#FFFFFF", "#006699"), style="diffusion")
```

The diffusion_output_rank is a ranking of the "hottest" ot the "coldest" nodes in the network when heat is being distributed from the selected top nodes to their neighbors.

> Q4: Looking at the diffusion_output_rank and heat attributes in the node table, which proteins in the network are most relevant for lung cancer? Does this align with the disease score in STRING?

