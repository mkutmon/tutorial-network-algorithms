# Network clustering in Cytoscape


## Preparation
Open RStudio and Cytoscape. Make sure you have the stringApp and clustermaker2 apps installed in Cytoscape.

Setup R workspace:

```
BiocManager::install(c("igraph", "RCy3", "gProfileR"))
library(RCy3)
library(igraph)
library(gProfileR)

cytoscapePing ()
```

For this example, we will use Michiel's co-occurence network for allelic imbalance in DCM. You can download the network session from the student portal (confidential). Open the Cytoscape session by double-clicking the downloaded .cys file.

```
networkSuid = getNetworkSuid()
```

> Q1: How many nodes and links are in the network?

> Q2: What is the meaning of the nodes and links in the network?

> Q3: How many connected components are in the network? Are all nodes connected?

> Q4: Do you see any clusters?


If you have the clustermaker2 app in Cytoscape installed, you can check which clustering algorithms are implemented in the app:
```
commandsHelp("help cluster")
```

You can use any of the algorithms with the code below by simply changing the clustering method in the first line (currently "cluster glay") - replace glay with mcode or mcl. 

----

## Community clustering

For community clustering, we will use the Girvan-Newman algorithm. It is a divisive algorithm where at each step the edge with the highest betweenness is removed from the graph. For each division you can compute the modularity of the graph. At the end, choose to cut the dendrogram where the process gives you the highest value of modularity.

![alt text](img/CommunityClustering.png)

M Newman and M Girvan: Finding and evaluating community structure in networks, Physical Review E 69, 026113 (2004). https://doi.org/10.1103/PhysRevE.69.026113

>  Q5: Explain the Girvan-Newman algorithm in your own word.

The code below, will perform the clustering and create subnetworks for each cluster with > 10 nodes. Additionally, it will perform a GO enrichment analysis for each cluster. 

This will take a while (especially GO enrichment!).
```
clustermaker <- paste("cluster glay createGroups=FALSE network=SUID:",networkSuid, sep="")
res <- commandsGET(clustermaker)
num <- as.numeric(gsub("Clusters: ", "", res[1]))

# for each cluster
for(i in 1:num) {
  setCurrentNetwork(network=networkSuid)
  clearSelection(network=networkSuid)
  current_cluster <- i
  selectednodes <- selectNodes(network=networkSuid, current_cluster, by.col="__glayCluster")
  if(length(selectednodes$nodes) > 10) {
    # Subnetwork creation
    subnetwork_suid <- createSubnetwork(nodes="selected", network=networkSuid)
    renameNetwork(paste("Glay_Cluster",i,"_Subnetwork", sep=""), network=as.numeric(subnetwork_suid))
    layoutNetwork()
    
    # GO analysis
    table <- getTableColumns(table="node",columns = "name", network = as.numeric(subnetwork_suid))
    gprofiler_results <- gprofiler(table$name, organism = "hsapiens", ordered_query = F, significant = F, max_set_size = 200, min_set_size = 3, exclude_iea = F, correction_method = "fdr", src_filter = c("GO:BP"))
    gprofiler_results <- gprofiler_results[which(gprofiler_results[,'term.size'] >= 3 & gprofiler_results[,'overlap.size'] >= 3 ),]
    assign(paste("Glay_GO_Cluster_", i, sep = ""), gprofiler_results)
  }
}
```

> Q6: How many clusters were found? Are you surprised by this result? Why / why not?

> Q7: What biological meaning do the clusters have? 

Continue with the other assignments. If you have more time at the end, feel free to try out different clustering algorithms. Work together and compare the results of the different algorithms.