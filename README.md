# Network algorithms

In two sessions, you will learn about different network algorithms, how they can be applied in biomedical research and which tools you can use to analyse your networks.

* [Part 1](Part1.md)
  * [Network clustering](NetworkClustering.md)
  * [Network motifs](NetworkMotifs.md)
  * [Network module identification](NetworkModules.md)
* [Part 2](Part2.md)
  * [Network propagation](NetworkPropagation.md)
  * [Network alignment](NetworkAlignment.md)
  * [Network-based classification](NetworkbasedClassification.md)
