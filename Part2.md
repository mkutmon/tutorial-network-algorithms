# Network Algorithms Part 2

Today you will use different tools to learn about network propagation, network alignment and network-based classification.

* [Network propagation](NetworkPropagation.md)
* [Network alignment](NetworkAlignment.md)
* [Network-based classification](NetworkbasedClassification.md)