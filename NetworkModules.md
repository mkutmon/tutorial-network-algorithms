# Active network modules

For this assignment, we will use the [jActiveModules app](http://apps.cytoscape.org/apps/jactivemodules) in Cytoscape. The algorithm has been [published in 2002](https://doi.org/10.1093/bioinformatics/18.suppl_1.S233) and used in many biomedical research examples. The original publication by Trey Ideker, the director of the National Resource for Network Biology (NRNB) has already been cited 1000+ times.

jActiveModules is an app that searches a molecular interaction network to find expression activated subnetworks. Such subnetworks are connected regions of a network that show significant changes in expression over particular subsets of conditions. The method combines a rigorous statistical measure for scoring subnetworks with a search algorithm for finding subnetworks with high score. Such expression-activated subnetworks have also been called 'network hotspots'.

The tutorial is adapted from: http://opentutorials.cgl.ucsf.edu/index.php/Tutorial:JActiveModules_3

Here is some background on the data you use in this tutorial. You are working with yeast, and the genes Gal1, Gal4, and Gal80 are all yeast transcription factors. Your expression experiments all involve some perturbation of these transcription factor genes. Gal1, Gal4, and Gal80 are also represented in your interaction network, where they are labeled according to yeast locus tags: Gal1 corresponds to YBR020W, Gal4 to YPL248C, and Gal80 to YML051W.

1. Install the jActiveModules App either from within Cytoscape 3 (Apps → App Manager) or directly from the [Cytoscape app store](http://apps.cytoscape.org/).
2. Open the galFiltered.cys (Cytoscape session file) in the provided sampleData directory (you can find it at the location where you installed Cytoscape, e.g. C:/Programs/Cytoscape_xxx/sampleData)
3. Open the jActiveModules tab in the control panel on the left. 
4. Select the "gal80Rexp" row in the table.
5. Expand the **Advanced** panel. Notice that the **Number of Modules** is set to 5. This means that five putative hits will be returned, even if only one good one is found.
6. Click **Search** to run jActiveModules. When the results are ready, you will see several new networks, one for each putative module and one network illustrating the search results, labeled jActiveModeuls Search Results 0, as shown below. You might not get exactly the same results in every run, because jActiveModules involves random sampling.

![alt text](img/jactivemodules.png "JActiveModules")


Answer the following questions:
> Q1: How many active modules were found and how large were they?

> Q2: Adapt the visual style (show gal80Rexp as fill color continuous mapping, change label to column "COMMON"). What can you say about the expression of the proteins in the modules? 

> Q3: Each module has an active path score. Can you find out what it means? 

> Q4: What would be possible next steps of the analysis? 

